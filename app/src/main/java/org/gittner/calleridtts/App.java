package org.gittner.calleridtts;

import android.app.Application;

import org.gittner.calleridtts.helpers.SettingsManager;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        SettingsManager.getInstance().init(this);
    }
}
