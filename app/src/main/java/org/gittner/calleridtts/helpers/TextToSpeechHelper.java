package org.gittner.calleridtts.helpers;

import android.content.Context;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;

/**
 * Class wrapper for the TextToSpeech Engine
 */
public class TextToSpeechHelper {

    private static final String TAG = "TextToSpeechHelper";

    private static final String SPEAK_COMPLETE = "SPEAK_COMPLETE";

    private Context mContext = null;

    private TextToSpeech mTts = null;

    private String mText = null;

    private int mRepetitions = 0;

    private int mRepetitionsDone = 0;

    private OnInitListener mOnInitListener = null;

    private OnPlaybackListener mPlaybackListener = null;

    /**
     * Interface for callbacks when the Engine is initialized
     */
    public interface OnInitListener {
        /**
         * Called when the Initialization was successful
         */
        public void onSuccess();

        /**
         * Called when te Initialization failed
         */
        public void onError();
    }

    /**
     * Interface for the playback that starts with the call to speak
     */
    public interface OnPlaybackListener {
        /**
         * Called when the playback was played completely without Errors
         */
        public void onDone();

        /**
         * Called when the playback failed
         */
        public void onError();
    }

    /**
     * Creates the TextToSpeech Engine. After Initialization the corresponding handler will be called.
     * @param context The Applications Context
     * @param onInitListener The Listener to be called when the Initialization is done
     * @param playbackListener The Listener to be called when the playback is done or failed
     */
    public TextToSpeechHelper(Context context, OnInitListener onInitListener, OnPlaybackListener playbackListener) {
        mContext = context;
        mOnInitListener = onInitListener;
        mPlaybackListener = playbackListener;

        mTts = new TextToSpeech(context, mTtsInitListener);
    }

    /**
     * Speak the given Text. Do not call this before the Engine is initialized!
     * @param text Text to be read out
     * @param repetitions How many times this text is repeated
     */
    public void speak(String text, int repetitions) {
        mText = text;
        mRepetitions = repetitions;

        if (mTts != null && mTts.isSpeaking()) {
            mTts.stop();
        }

        speakText();
    }

    /**
     * Stop the Engine and release its resources
     */
    public void release()
    {
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
        mTts = null;
    }

    private void speakText()
    {
        mRepetitionsDone ++;
        if(mRepetitionsDone < mRepetitions
                || mRepetitions == 0)
        {
            HashMap<String, String> params = new HashMap<>();
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, SPEAK_COMPLETE);
            params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_RING));

            //noinspection deprecation
            if(mTts.speak(mText, TextToSpeech.QUEUE_FLUSH, params) != TextToSpeech.SUCCESS)
            {
                Log.e(TAG, "Failed to speak Text");
                mPlaybackListener.onError();
            }
        }
        else
        {
            mPlaybackListener.onDone();
        }
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final TextToSpeech.OnInitListener mTtsInitListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (status != TextToSpeech.SUCCESS) {
                Log.e(TAG, "Failed to initialize TTS");
                mOnInitListener.onError();
                return;
            }

            int langAvailable = mTts.isLanguageAvailable(LocaleHelper.getBestLocale(mContext));
            if(langAvailable == TextToSpeech.LANG_NOT_SUPPORTED
                    || langAvailable == TextToSpeech.LANG_MISSING_DATA)
            {
                Log.e(TAG, "Best locale not available");
            }
            else
            {
                mTts.setLanguage(LocaleHelper.getBestLocale(mContext));
            }

            //noinspection deprecation
            mTts.setOnUtteranceCompletedListener(mUtteranceCompletedListener);

            mOnInitListener.onSuccess();
        }
    };

    private final TextToSpeech.OnUtteranceCompletedListener mUtteranceCompletedListener = new TextToSpeech.OnUtteranceCompletedListener() {
        @Override
        public void onUtteranceCompleted(String utteranceId) {
            if(SPEAK_COMPLETE.equals(utteranceId))
            {
                speakText();
            }
            else
            {
                mPlaybackListener.onError();
            }
        }
    };
}
