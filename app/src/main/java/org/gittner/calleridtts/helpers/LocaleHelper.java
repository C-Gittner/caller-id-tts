package org.gittner.calleridtts.helpers;

import android.content.Context;

import org.gittner.calleridtts.R;

import java.util.Locale;

/**
 * Class with various functions for the Locale Management
 */
class LocaleHelper {

    /**
     * Returns the best Locale by a lookup in the strings resource file for the Entry "locale"
     * @param context The Context of the running App
     * @return The Locale found or the Apps default locale (EN)
     */
    public static Locale getBestLocale(Context context) {
        return new Locale(context.getString(R.string.locale));
    }
}
