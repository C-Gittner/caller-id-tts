package org.gittner.calleridtts.helpers;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.media.AudioManager;
import android.util.Log;

/**
 * Helper Class for various Bluetooth functions
 */
public class BluetoothHelper {

    private static final String TAG = "BluetoothHelper";

    /**
     * Checks if Bluetooth is Available
     * @return True if Bluetooth is available
     */
    public static boolean isBluetoothAvailable()
    {
        return BluetoothAdapter.getDefaultAdapter() != null;
    }

    /**
     * Retrieve information about whether a Bluetooth Headset is connected via A2dp
     * @param context The Context of the running App
     * @return True if a Headset is connected
     */
    public static boolean headsetConnected(Context context)
    {
        AudioManager manager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if(manager == null)
        {
            Log.e(TAG, "Failed to get AudioManager service");
            return false;
        }

        return manager.isBluetoothA2dpOn();
    }
}
