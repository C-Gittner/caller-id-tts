package org.gittner.calleridtts.helpers;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * Class for various Contact related functions
 */
public class ContactHelper {

    /**
     * Retrieves the first Display name of a Contact from a Phone number
     * @param context The Context of the running App
     * @param number The Phone number of the contact
     * @return The retrieved Display Name or "" if no number is found
     */
    public static String getContactDisplayNameByNumber(Context context, String number)
    {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(
                uri
                , new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}
                , ContactsContract.PhoneLookup.DISPLAY_NAME + "='" + number + "'"
                , null
                , null);

        cursor.moveToFirst();

        if (!cursor.isAfterLast()) {
            String displayName = cursor.getString(0);
            cursor.close();
            return displayName;
        }

        cursor.close();

        return "";
    }
}
