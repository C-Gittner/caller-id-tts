package org.gittner.calleridtts.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.gittner.calleridtts.R;

@SuppressWarnings("UnusedDeclaration")
public class SettingsManager {

    private static String KEY_ENABLE;
    private static boolean DEF_ENABLE;

    private static String KEY_ONLY_BLUETOOTH_HEADSET;
    private static boolean DEF_ONLY_BLUETOOTH_HEADSET;

    private static String KEY_REPETITIONS;
    private static String DEF_REPETITIONS;

    private static final SettingsManager Instance = new SettingsManager();

    private SharedPreferences mManager = null;

    public void init(Context context)
    {
        mManager = PreferenceManager.getDefaultSharedPreferences(context);

        KEY_ENABLE = context.getString(R.string.pref_enable_key);
        DEF_ENABLE = Boolean.valueOf(context.getString(R.string.pref_enable_def));

        KEY_ONLY_BLUETOOTH_HEADSET = context.getString(R.string.pref_only_bt_headset_key);
        DEF_ONLY_BLUETOOTH_HEADSET = Boolean.valueOf(context.getString(R.string.pref_only_bt_headset_def));

        KEY_REPETITIONS = context.getString(R.string.pref_repetitions_key);
        DEF_REPETITIONS = context.getString(R.string.pref_repetitions_def);
    }

    public static SettingsManager getInstance() {
        return Instance;
    }

    private SettingsManager() {
    }

    public boolean getEnabled()
    {
        return mManager.getBoolean(KEY_ENABLE, DEF_ENABLE);
    }

    public void setEnabled(boolean value)
    {
        mManager.edit().putBoolean(KEY_ENABLE, value).apply();
    }

    public boolean getOnlyBluetoothHeadset()
    {
        return BluetoothHelper.isBluetoothAvailable()
                && mManager.getBoolean(KEY_ONLY_BLUETOOTH_HEADSET, DEF_ONLY_BLUETOOTH_HEADSET);
    }

    public void setOnlyBluetoothHeadset(boolean value)
    {
        mManager.edit().putBoolean(KEY_ONLY_BLUETOOTH_HEADSET, value).apply();
    }

    public int getRepetitions()
    {
        return Integer.valueOf(mManager.getString(KEY_REPETITIONS, DEF_REPETITIONS));
    }

    public void setRepetitions(int value)
    {
        mManager.edit().putString(KEY_REPETITIONS, String.valueOf(value)).apply();
    }
}
