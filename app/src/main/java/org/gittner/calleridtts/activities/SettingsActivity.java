package org.gittner.calleridtts.activities;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;

import org.gittner.calleridtts.R;
import org.gittner.calleridtts.helpers.BluetoothHelper;
import org.gittner.calleridtts.helpers.IntentHelper;

public class SettingsActivity extends PreferenceActivity {

    private static final String TAG = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //noinspection deprecation
        addPreferencesFromResource(R.xml.preferences);

        //noinspection deprecation
        findPreference(getString(R.string.pref_send_feedback_key)).setOnPreferenceClickListener(mPreferenceClickListener);

        /* Disable the Bluetooth Headset Preference if no Bluetooth is available */
        if(!BluetoothHelper.isBluetoothAvailable())
        {
            //noinspection deprecation
            findPreference(getString(R.string.pref_only_bt_headset_key)).setEnabled(false);
            //noinspection deprecation
            findPreference(getString(R.string.pref_only_bt_headset_key)).setSummary(getString(R.string.bluetooth_is_not_supported_on_this_device));
        }
    }

    private final Preference.OnPreferenceClickListener mPreferenceClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            if (getString(R.string.pref_send_feedback_key).equals(preference.getKey()))
            {
                onFeedbackClicked();
                return true;
            }

            return false;
        }
    };

    private void onFeedbackClicked()
    {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.developer_mail)});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_to_caller_id_tts));
        emailIntent.setType("plain/text");

        if(IntentHelper.intentHasReceivers(this, emailIntent))
        {
            try {
                startActivity(Intent.createChooser(emailIntent, getString(R.string.email_feedback)));
            }
            catch (ActivityNotFoundException e)
            {
                Log.e(TAG, "No Email Activity found: " + e.getMessage());
                e.printStackTrace();
                showSendFeedbackErrorDialog();
            }
        }
        else
        {
            showSendFeedbackErrorDialog();
        }
    }

    private void showSendFeedbackErrorDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.sending_feedback_failed_title);
        builder.setMessage(R.string.sending_feedback_failed_message);
        builder.setCancelable(true);
        builder.create().show();
    }
}
