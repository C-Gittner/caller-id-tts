package org.gittner.calleridtts.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.gittner.calleridtts.services.IncomingCallService;

import java.util.ArrayList;

public class CallBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "CallBroadCastReceiver";

    private static boolean isRinging = false;

    /**
     * Listener to get notified when the Phone stops ringing
     */
    public interface OnRingingEndedListener {
        public void onRingingEnded();
    }

    private static final ArrayList<OnRingingEndedListener> mListeners = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {

        if (TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(intent.getAction())) {
            if(!isRinging
                    && TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE)))
            {
                isRinging = true;
                if (intent.hasExtra(TelephonyManager.EXTRA_INCOMING_NUMBER)) {
                    String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    onRingingStart(context, number);
                }
                else
                {
                    onRingingStart(context);
                }
            }
            else if(isRinging
                    && TelephonyManager.EXTRA_STATE_IDLE.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE)))
            {
                isRinging = false;
                onRingingEnded();
            }
            else if(isRinging
                    && TelephonyManager.EXTRA_STATE_OFFHOOK.equals(intent.getStringExtra(TelephonyManager.EXTRA_STATE)))
            {
                isRinging = false;
                onRingingEnded();
            }
        }
    }

    /**
     * Called when the Phone starts to ring and the number calling is not known
     * @param context the Context in which the Receiver is running
     */
    private void onRingingStart(Context context)
    {
        Log.d(TAG, "Call from Unknown Number");

        Intent intent = new Intent(context, IncomingCallService.class);

        context.startService(intent);
    }

    /**
     * Called when the Phone starts to ring and the number calling is known
     * @param context the Context in which the Receiver is running
     * @param number the Number calling
     */
    private void onRingingStart(Context context, String number)
    {
        Log.d(TAG, "Call from " + number);

        Intent intent = new Intent(context, IncomingCallService.class);
        intent.putExtra(IncomingCallService.EXTRA_NUMBER, number);

        context.startService(intent);
    }

    /**
     * Called when the Phone stops Ringing
     */
    private void onRingingEnded()
    {
        Log.d(TAG, "Ringing ended");

        for(OnRingingEndedListener listener : mListeners)
        {
            listener.onRingingEnded();
        }
    }

    /**
     * Adds a Listener to be called when the Phone is no longer Ringing
     * @param listener the listener to be added
     */
    public static void addOnRingingEndedListener(OnRingingEndedListener listener)
    {
        mListeners.add(listener);
    }

    /**
     * Removes a Listener to be called when the Phone is no longer Ringing
     * @param listener the Listener to be removed
     */
    public static void removeOnRingingEndedListener(OnRingingEndedListener listener)
    {
        mListeners.remove(listener);
    }
}
