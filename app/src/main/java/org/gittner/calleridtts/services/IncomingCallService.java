package org.gittner.calleridtts.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.gittner.calleridtts.R;
import org.gittner.calleridtts.broadcastreceivers.CallBroadcastReceiver;
import org.gittner.calleridtts.helpers.BluetoothHelper;
import org.gittner.calleridtts.helpers.ContactHelper;
import org.gittner.calleridtts.helpers.SettingsManager;
import org.gittner.calleridtts.helpers.TextToSpeechHelper;

public class IncomingCallService extends Service {

    private static final String TAG = "IncomingCallService";

    public static final String EXTRA_NUMBER = "EXTRA_NUMBER";

    private String number = "";

    private TextToSpeechHelper mTts = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.hasExtra(EXTRA_NUMBER)) {
            number = intent.getStringExtra(EXTRA_NUMBER);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate()");

        if(!SettingsManager.getInstance().getEnabled())
        {
            Log.d(TAG, "App is not enabled");
            stopSelf();
            return;
        }

        CallBroadcastReceiver.addOnRingingEndedListener(mOnRingingEndedListener);

        if(SettingsManager.getInstance().getOnlyBluetoothHeadset()
                && !BluetoothHelper.headsetConnected(getApplicationContext()))
        {
            Log.d(TAG, "No Headset connected");
            stopSelf();
            return;
        }

        mTts = new TextToSpeechHelper(this, mOnInitListener, mPlaybackListener);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");

        if(mOnRingingEndedListener != null)
        {
            CallBroadcastReceiver.removeOnRingingEndedListener(mOnRingingEndedListener);
        }

        if(mTts != null)
        {
            mTts.release();
        }
        mTts = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final TextToSpeechHelper.OnInitListener mOnInitListener = new TextToSpeechHelper.OnInitListener() {
        @Override
        public void onSuccess() {
            if (!number.equals(""))
            {
                String displayName = ContactHelper.getContactDisplayNameByNumber(getApplicationContext(), number);
                if(!displayName.equals(""))
                {
                    mTts.speak(
                            getString(R.string.call_from) + displayName,
                            SettingsManager.getInstance().getRepetitions());
                    return;
                }
            }

            mTts.speak(
                    getString(R.string.unknown_caller)
                    , SettingsManager.getInstance().getRepetitions());
        }

        @Override
        public void onError() {
            stopSelf();
        }
    };

    private final TextToSpeechHelper.OnPlaybackListener mPlaybackListener = new TextToSpeechHelper.OnPlaybackListener() {
        @Override
        public void onDone() {
            stopSelf();
        }

        @Override
        public void onError() {
            stopSelf();
        }
    };

    private final CallBroadcastReceiver.OnRingingEndedListener mOnRingingEndedListener = new CallBroadcastReceiver.OnRingingEndedListener() {
        @Override
        public void onRingingEnded() {
            stopSelf();
        }
    };
}
